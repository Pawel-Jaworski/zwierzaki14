package pl.tomasz.smutek;

import pl.tomasz.smutek.jedzenie.Jedzenie;

import java.util.ArrayList;
import java.util.List;

public class PojemnikNaJedzenie {

    List<Jedzenie> jedzenie = new ArrayList<>();

    public void dodajJedzenie(Jedzenie jedzenieDoDodania){
        int indexJedzenia = jedzenie.indexOf(jedzenieDoDodania);

        if (indexJedzenia >= 0){
            Jedzenie jedzenieZPojemnika = jedzenie.get(indexJedzenia);
            jedzenieZPojemnika.dodajJedzenie(jedzenieDoDodania);
        } else {
            jedzenie.add(jedzenieDoDodania);
        }
    }

    @Override
    public String toString() {
        return jedzenie.toString();
    }

    public int ilePosilkow() {
        return jedzenie.size();
    }

    public Jedzenie wezPosilek(int index) {
        return jedzenie.get(index);
    }

    /**
     * Metod ma za zadanie wyjąć z pojemnika zadane jedzenie
     *
     * @param coChceZjesc jedzenie które chcemy wyjąć z pojemnika
     *
     * @return jedzenie ile nie udało się wyjać z pojemnika
     */
    public Jedzenie wyjmijZPojemnika(Jedzenie coChceZjesc) {
        return null;
    }
}
