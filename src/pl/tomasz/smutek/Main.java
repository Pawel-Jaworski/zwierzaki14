package pl.tomasz.smutek;

import pl.tomasz.smutek.jedzenie.Jedzenie;
import pl.tomasz.smutek.jedzenie.TypJedzenia;
import pl.tomasz.smutek.zwierz.Pies;
import pl.tomasz.smutek.zwierz.Ptak;

public class Main {

    public static void main(String[] args) {

        Osoba tomek = new Osoba("Tomek");

        // zwierzeta
        Pies burek = new Pies("burek");
        Ptak cwirek = new Ptak("Ćwirek");
        tomek.dodajZwierzaka(burek);
        tomek.dodajZwierzaka(cwirek);

        // napełnienie lodówki
        Jedzenie mlekoDoLodowki = new Jedzenie(TypJedzenia.MLEKO, 4);
        Jedzenie miesoDoLodowki = new Jedzenie(TypJedzenia.MIESO, 4);
        Jedzenie ziarnoDoLodowki = new Jedzenie(TypJedzenia.ZIARNO, 14);
        Jedzenie zoltySerDoLodowki = new Jedzenie(TypJedzenia.ZOLTY_SER, 14);

        tomek.dodajDoLodowki(mlekoDoLodowki);
        tomek.dodajDoLodowki(miesoDoLodowki);
        tomek.dodajDoLodowki(ziarnoDoLodowki);
        tomek.dodajDoLodowki(zoltySerDoLodowki);
        tomek.dodajDoLodowki(miesoDoLodowki);


        // głód dla psa
        Jedzenie miesoGlodPsa = new Jedzenie(TypJedzenia.MIESO, 2);
        Jedzenie ziarnoGlodPsa = new Jedzenie(TypJedzenia.ZIARNO, 4);
        burek.dodajGlod(miesoGlodPsa);
        burek.dodajGlod(ziarnoGlodPsa);

        // głód dla ptaka
        Jedzenie mlekoDlaPtaka = new Jedzenie(TypJedzenia.MLEKO, 4);
        Jedzenie serDlaPtaka = new Jedzenie(TypJedzenia.ZOLTY_SER, 1);
        cwirek.dodajGlod(mlekoDlaPtaka);
        cwirek.dodajGlod(serDlaPtaka);

        // wyświetlenie przed karmieniem
        System.out.println("----------------PRZED KARMIENIEM----------------");
        System.out.println(tomek.przedstawSie());

        tomek.nakarmZwierzaki();

        // wyświetlenie po karmieniu
        System.out.println("----------------PO KARMIENIU----------------");
        System.out.println(tomek.przedstawSie());


    }
}
