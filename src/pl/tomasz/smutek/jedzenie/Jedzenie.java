package pl.tomasz.smutek.jedzenie;

import java.util.Objects;

public class Jedzenie {

    TypJedzenia typJedzenia;
    int ilosc;

    public Jedzenie(TypJedzenia typJedzenia, int ilosc) {
        this.typJedzenia = typJedzenia;
        this.ilosc = ilosc;
    }

    public void dodajJedzenie(Jedzenie jedzenieDoDodania) {
        ilosc += jedzenieDoDodania.ilosc;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Jedzenie)) return false;
        Jedzenie jedzenie = (Jedzenie) o;
        return typJedzenia == jedzenie.typJedzenia;
    }

    @Override
    public int hashCode() {
        return Objects.hash(typJedzenia);
    }

    @Override
    public String toString() {
        return typJedzenia.toString() + ": " + ilosc;
    }
}
