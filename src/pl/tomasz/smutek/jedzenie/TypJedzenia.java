package pl.tomasz.smutek.jedzenie;

public enum TypJedzenia {

    ZIARNO("ziarno"),
    MIESO("mięso"),
    MLEKO("mleko"),
    ZOLTY_SER("żółty ser"),
    CHLEB("chleb");

    String mojaNazwa;

    TypJedzenia(String nazwa){
        mojaNazwa = nazwa;
    }

    @Override
    public String toString() {
        return mojaNazwa;
    }
}
