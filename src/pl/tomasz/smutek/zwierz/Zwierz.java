package pl.tomasz.smutek.zwierz;

import pl.tomasz.smutek.Osoba;
import pl.tomasz.smutek.PojemnikNaJedzenie;
import pl.tomasz.smutek.jedzenie.Jedzenie;

import java.util.ArrayList;
import java.util.List;

public abstract class Zwierz {
    String imie;
    PojemnikNaJedzenie glod = new PojemnikNaJedzenie();

    public Zwierz(String imie) {
        super();
        this.imie = imie;
    }

    public String przedstawSie(){
        return  rodzajZwierzaka() + " " + imie + ", chce zjeść: " + glod.toString();
    }

    protected abstract String rodzajZwierzaka();

    public abstract String dajGlos();

    @Override
    public String toString() {
        return przedstawSie();
    }

    public void dodajGlod(Jedzenie jedzenieDoDodania) {
        glod.dodajJedzenie(jedzenieDoDodania);
    }

    public PojemnikNaJedzenie pobierzGlod() {
        return glod;
    }
}
