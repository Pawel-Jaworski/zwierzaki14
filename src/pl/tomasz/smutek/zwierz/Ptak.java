package pl.tomasz.smutek.zwierz;

public class Ptak extends Zwierz {

    public Ptak(String imie) {
        super(imie);
    }

    @Override
    protected String rodzajZwierzaka() {
        return "ptak";
    }


    @Override
    public String dajGlos() {
        return "ćwir ćwir";
    }
}
