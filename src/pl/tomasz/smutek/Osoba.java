package pl.tomasz.smutek;

import pl.tomasz.smutek.jedzenie.Jedzenie;
import pl.tomasz.smutek.zwierz.Zwierz;

import java.util.ArrayList;
import java.util.List;

public class Osoba {
    String imie;
    List<Zwierz> mojeZwierzaki = new ArrayList<>();
    PojemnikNaJedzenie lodowka = new PojemnikNaJedzenie();
    PojemnikNaJedzenie listaZakupow = new PojemnikNaJedzenie();

    public Osoba(String imie) {
        this.imie = imie;
    }

    public void zrobZakupy(){

    }

    public void nakarmZwierzaki(){

        for(Zwierz zwierzakDoNakarmienia : mojeZwierzaki){

            PojemnikNaJedzenie glod = zwierzakDoNakarmienia.pobierzGlod();

            for(int index = 0; index < glod.ilePosilkow(); index++){
                Jedzenie coChceZjesc = glod.wezPosilek(index);

                lodowka.wyjmijZPojemnika(coChceZjesc);
            }

        }

    }

    public String przedstawSie(){
        return toString();
    }

    public void dodajZwierzaka(Zwierz zwierz) {
        mojeZwierzaki.add(zwierz);
    }

    public void dodajDoLodowki(Jedzenie jedzenieDoDodania) {
        lodowka.dodajJedzenie(jedzenieDoDodania);
    }


    public void dodajDoListyZakupow(Jedzenie jedzenieDoDodania) {
        listaZakupow.dodajJedzenie(jedzenieDoDodania);
    }

    @Override
    public String toString() {
        return "Mam na imie: " + imie + ".\nMam: " + mojeZwierzaki.size() +
                " zwierzaków.\nSą to: " + mojeZwierzaki.toString() +
                ".\nW lodówce mam: " + lodowka.toString() + ".\nPowinienem kupić: " +
                listaZakupow.toString();
    }
}
